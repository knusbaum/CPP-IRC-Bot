#include "IRCHandler.hpp"


class KickHandler : public IRCHandler {
public:
    virtual bool handle(IRCState &s, IRCMessage &m, std::ostream& o) {
        if(m.command == "KICK"
           && m.params.size() >= 2
           && m.params[1] == "knusbaum_bot") {
            o << "JOIN " << m.params[0] << "\r\n";
            return true;
            
        }
        return false;
    }
    
    virtual std::string get_name() const { return "KickHandler"; }
};



extern "C" IRCHandler *create() {
    return new KickHandler();
}

extern "C" void destroy (KickHandler *h) {
    delete h;
}
