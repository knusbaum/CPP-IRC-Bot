#ifndef PROCESSPOOL_H
#define PROCESSPOOL_H

#include <functional>
#include <iostream>
#include <vector>
#include <memory>
#include <mutex>
#include "Process.hpp"


class ProcessPool {

    
public:
    ~ProcessPool();
    std::shared_ptr<Process>
    exec_in_process(std::function
                    <void(std::shared_ptr<std::istream> in,
                          std::shared_ptr<std::ostream> out)>);
    void respawn();
    
    std::vector<std::shared_ptr<Process>> processes;

private:
    void exec_in_process(std::function
                         <void(std::shared_ptr<std::istream> in,
                               std::shared_ptr<std::ostream> out)> fun,
                         std::shared_ptr<Process> p);
    void spawn_group_leader();
    void killall();
    Process * group_leader = NULL;
    pid_t gid = 0;
};

#endif
