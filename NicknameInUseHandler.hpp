#ifndef NICKNAME_IN_USE_HANDLER_H
#define NICKNAME_IN_USE_HANDLER_H

#include "IRCHandler.hpp"

class NicknameInUseHandler : public IRCHandler {
public:
    virtual bool handle(IRCState &s, IRCMessage& m, std::ostream& o) {
        if(m.command == "433" && m.trailing == "Nickname is already in use.") {
            std::cout << get_name() << " Nickname is already in use. Exiting." << std::endl;
            exit(0);
        }
        return false;
    }

    virtual std::string get_name() const { return "NicknameInUseHandler"; }
};

#endif
