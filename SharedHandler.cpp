#include <sstream>
#include <signal.h>
#include "SharedHandler.hpp"
#include "IRCParser.hpp"
#include "IRCHandler.hpp"
#include "dlclass.hpp"

enum class MSGType {
    MESSAGE,
    COMMAND
};

MSGType scan_next(std::istream& in);

SharedHandler::SharedHandler(std::string shared_object_file, ProcessPool& p) {
    signal(SIGPIPE, SIG_IGN);
    
    process = p.exec_in_process(
        [shared_object_file]
        (std::shared_ptr<std::istream> i,
         std::shared_ptr<std::ostream> o){
            
            std::shared_ptr<std::istream> in = i;
            std::shared_ptr<std::ostream> out = o;

            IRCState s(*out);
            
            DLClass<IRCHandler> dlc(shared_object_file);
            auto module_ptr = dlc.make_obj();
            if(!module_ptr) {
                std::cout << " Exiting. Failed to load library: " << shared_object_file
                          << std::endl;
                exit(1);
            }
            
            while(in->good()) {
                MSGType mt = scan_next(*in);
                if(mt == MSGType::MESSAGE) {

                    IRCMessage m(*in);
//                    std::stringstream ss;
                    s.update_with(m);
                    if(module_ptr->handle(s, m, *out)) {
                        (*out) << std::string("t")
                               << std::endl;
                        out->flush();
//                        std::cout << module_ptr->get_name() << " Sending: ["
//                                  << ss.str()
//                                  << std::string("t")
//                                  << "]"<< std::endl;
                    }
                    else {
                        (*out) << std::string("f")
                               << std::endl;
                        out->flush();
//                        std::cout << module_ptr->get_name() << " Sending: ["
//                                  << ss.str()
//                                  << std::string("f")
//                                  << "]"<< std::endl;
                    }
                }
                else if(mt == MSGType::COMMAND) {
                    
                    char buff[1024];
                    in->getline(buff, 1024);
                    std::string command(buff);
                    if(command == "get_name") {
                        (*out) << module_ptr->get_name() << std::endl;
                        out->flush();
                    }
                    else {
                        (*out) << "nil" << std::endl;
                         out->flush();
                    }
                }
            }
        });
}

std::string SharedHandler::get_name() const {
    if(process->out_stream->good()) {
        (*process->out_stream) << "c" << std::endl << "get_name" << std::endl;
        process->out_stream->flush();
        
        char buff[1024];
        process->in_stream->getline(buff, 1024);
        std::stringstream ss;
        ss << "SharedHandler<" << buff << ">";
        return ss.str();
    }
    return "DEFUNCT";
}

bool SharedHandler::handle(IRCState &s, IRCMessage& m, std::ostream& o) {

    if(process->out_stream->good()) {
        (*process->out_stream) << "m" << std::endl << m.serialize();
        process->out_stream->flush();
        char buff[1024];
        bool good = false;

        while(process->in_stream->good()) {
            process->in_stream->getline(buff, 1024);
            if(strcmp(buff, "t") == 0) {
                return true;
            }
            else if(strcmp(buff, "f") == 0) {
                return false;
            }
            
            //   std::cout << "Sending: [" << buff << "]"<< std::endl;
            o << buff;
        }
    }
    return false;
}

MSGType scan_next(std::istream& in) {
    char buff[1024];
    while(in.good()) {
        in.getline(buff, 1024);
        if(buff[0] == 'm') {
            return MSGType::MESSAGE;
        }
        else if(buff[0] == 'c') {
            return MSGType::COMMAND;
        }
    }
}
