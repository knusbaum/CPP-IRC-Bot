#ifndef PINGHANDLER_H
#define PINGHANDLER_H

#include "IRCHandler.hpp"
#include <iostream>

class PingHandler : public IRCHandler {

    virtual bool handle(IRCState &s, IRCMessage& m, std::ostream& o) {
        if(m.command == "PING") {
            o << "PONG " << m.trailing << "\r\n";
            return true;
        }
        return false;
    }

    virtual std::string get_name() const { return "PingHandler"; }
    
};

#endif
