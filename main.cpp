#include <iostream>
#include <boost/asio.hpp>
#include <memory>
#include "IRCState.hpp"
#include "IRCParser.hpp"
#include "IRCHandler.hpp"
#include "PingHandler.hpp"
#include "HelloHandler.cpp"
#include "SharedHandler.hpp"
#include "ProcessPool.hpp"
#include "NicknameInUseHandler.hpp"
#include "KillHandler.hpp"

std::string http_request(std::string host, std::string path);

int main(void) {
    boost::asio::ip::tcp::iostream stream("localhost", "8990");
    if(!stream)
    {
        std::cout << "Can't connect." << std::endl;
    }

    stream << "PASS GmRt9amhFWXU\r\n";
    stream << "NICK knusbaum_bot\r\n";
    stream << "USER knusbaum 0 * :Kyle Nusbaum\r\n";
    stream << "JOIN #ystorm_test\r\n";
    stream << "JOIN #ystorm\r\n";
    
    IRCParser p;
    IRCMessage m;
    IRCState s(stream);
    s.add_channel("#ystorm_test");
    s.add_channel("#ystorm");
    
    ProcessPool pp;
    
    std::vector<std::shared_ptr<IRCHandler>> handlers;
    handlers.push_back(std::make_shared<PingHandler>());
    handlers.push_back(std::make_shared<NicknameInUseHandler>());
    handlers.push_back(std::make_shared<KillHandler>());
    handlers.push_back(std::make_shared<SharedHandler>("HelloHandler.so", pp));
    handlers.push_back(std::make_shared<SharedHandler>("KickHandler.so", pp));
    handlers.push_back(std::make_shared<SharedHandler>("CrashCausingHandler.so", pp));
    handlers.push_back(std::make_shared<SharedHandler>("FactoidHandler.so", pp));
    handlers.push_back(std::make_shared<SharedHandler>("OPHandler.so", pp));
    
    while(stream.good()) {
        bool handled = false;
        m = p.parse(stream);
        pp.respawn();
        
        s.update_with(m);
        
        for(auto handler : handlers) {
            if(handler->handle(s, m, stream)) {
                handled = true;
                std::cout << handler->get_name()
                          << " handled [" << m.command << "]" << std::endl; 
            }
        }
        if(!handled) {
          std::cout << "Not Handled: [" << m.command << "] "
		    << m.trailing << std::endl;
        }
    }
    std::cout << "Exiting." << std::endl;    
    p.dump_stream(stream);
}
