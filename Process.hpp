#ifndef PROCESS_H
#define PROCESS_H

#include <memory>
#include <iostream>
#include <fstream>

class Process {
public:
    Process(std::function<void(std::shared_ptr<std::istream> in, std::shared_ptr<std::ostream> out)> fun,
            std::shared_ptr<std::istream> in,
            std::shared_ptr<std::ostream> out,
            pid_t pid)
        : function(fun),
	  in_stream(in),
          out_stream(out),
          child_pid(pid)
        {}
    Process() : in_stream(nullptr),
                out_stream(nullptr),
                child_pid(0) {}
    Process(Process&&) = default;
    ~Process() { }
    std::shared_ptr<std::istream> in_stream;
    std::shared_ptr<std::ostream> out_stream;

    void reset(std::function<void(std::shared_ptr<std::istream> in, std::shared_ptr<std::ostream> out)> fun,
               std::shared_ptr<std::istream> in,
               std::shared_ptr<std::ostream> out,
               pid_t pid) {
        function = fun;
        in_stream = in;
        out_stream = out;
        child_pid = pid;
    }
    bool kill();
    bool wait();
    pid_t get_pid() { return child_pid; }
    std::function<void(std::shared_ptr<std::istream> in, std::shared_ptr<std::ostream> out)> get_function() { return function; }
    
private:
    Process(const Process& other);
    Process& operator=(const Process&);
    pid_t child_pid;
    
    std::function<void(std::shared_ptr<std::istream> in, std::shared_ptr<std::ostream> out)> function;
};

#endif
