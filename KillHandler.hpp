#ifndef KILL_HANDLER_H
#define KILL_HANDLER_H

#include "IRCHandler.hpp"

class KillHandler : public IRCHandler {
public:
    virtual bool handle(IRCState &s, IRCMessage& m, std::ostream& o) {
        if(m.command == "PRIVMSG"
           && m.trailing == "!kill") {
            m.print();
            std::cout << "Operator? " << s.is_operator(strip_nick(m.prefix), m.params[0]) << std::endl;
            if(s.is_operator(strip_nick(m.prefix), m.params[0]))
                exit(0);
        }
        return false;
    }
    
    virtual std::string get_name() const { return "KillHandler"; }
};

#endif
