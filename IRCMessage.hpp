#ifndef IRCMESSAGE_H
#define IRCMESSAGE_H

#include <iostream>
#include <vector>

struct IRCMessage {
    std::string prefix;
    std::string command;
    std::vector<std::string> params;
    std::string trailing;
    
    IRCMessage() : prefix(""), command(""), params() {}

    // Deserializing Constructor
    IRCMessage(std::istream& in);
    std::string serialize();

    void print();
    void reply(std::ostream &o, std::string response);
    
private:
    bool deserialize_field_into(std::istream& in, char indicator, std::string& str);
    void scan_for_next(std::istream& in);
};


#endif
