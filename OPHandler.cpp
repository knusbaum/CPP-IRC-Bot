#include "IRCHandler.hpp"
#include <unordered_map>
#include <string>
#include <fstream>
#include <sstream>
#include <regex>
#include <set>

class OPHandler : public IRCHandler {

    std::set<std::string> ops;
    std::fstream opsFile;

public:

    OPHandler() {
        std::cout << get_name() << " Loading ..." << std::endl;
        opsFile.open("ops", std::ios::in);
        if(opsFile.is_open()) {
            std::string name;
            while(getline(opsFile, name)) {
                std::cout << get_name() << " Found OP: [" << name << "]" << std::endl;
                ops.insert(name);
            }
            std::cout << get_name() << " Done Getting Lines." << std::endl;
        }
        else {
            std::cout << get_name() << " Failed to load ops." << std::endl;
        }

        opsFile.close();
        std::cout << get_name() << " Done." << std::endl;
    }

    virtual ~OPHandler() {
        opsFile.close();
        ops.empty();
    }

    bool nickChange(IRCState &s, std::ostream &o, std::string nick) {
        if(ops.find(nick) == ops.end()) {
            nick = "-" + nick;
        }

        auto cset = s.channel_set();
        auto it = cset.begin();
        for(; it != cset.end(); ++it) {
            o << "PRIVMSG ChanServ :OP " << *it << " " << nick << "\r\n";
        }
        
        return true;
    }

    void writeOps() {
        opsFile.open("ops", std::ios::out);
        auto it = ops.begin();
        for(; it != ops.end(); ++it) {
            std::string opname = *it;
            opsFile << opname << std::endl;
        }
        opsFile.flush();
    }
    
    bool doOpAdd(IRCState &s, IRCMessage &m, std::ostream& o) {
        try {
            std::regex re("(addop +([^ ]*))$");
            std::smatch match;
            if(std::regex_search(m.trailing, match, re)
               && match.size() > 2) {
                if(s.is_operator(strip_nick(m.prefix), m.params[0])) {
                    std::string opname = match.str(2);
                    if(opname.length() > 0) {
                        ops.insert(opname);
                        writeOps();
                        m.reply(o, "Added " + opname + " to oplist.");
                        std::cout << "Oplist: " << opsString(o);
                        return true;
                    }
                }
                else {
                    m.reply(o, "Must be an operator to do that.");
                }
            }
        }
        catch (std::regex_error& e) {
            std::cout << get_name() << " Bad Regex." << std::endl;
        }
        return false;
    }

    bool doOpDel(IRCState &s, IRCMessage &m, std::ostream &o) {
        try {
            std::regex re("(delop +([^ ]*))$");
            std::smatch match;
            if(std::regex_search(m.trailing, match, re)
               && match.size() > 2) {
                if(s.is_operator(strip_nick(m.prefix), m.params[0])) {
                    std::string opname = match.str(2);
                    if(opname.length() > 0) {
                        ops.erase(ops.find(opname));
                        writeOps();
                        m.reply(o, "Removed " + opname + " from oplist. ");
                        std::cout << "Oplist: " << opsString(o);
                        return true;
                    }
                }
                else {
                    m.reply(o, "Must be an operator to do that.");
                }
            }
        }
        catch (std::regex_error& e) {
            std::cout << get_name() << " Bad Regex." << std::endl;
        }
        return false;
    }

    std::string opsString(std::ostream &o) {
        std::stringstream ss;
        bool first = true;
        auto it = ops.begin();

        
        ss << "[ ";
        for(; it != ops.end(); ++it) {
            if(first) first = !first;
            else ss << ", ";
            ss << *it;
        }
        ss << "]";

        return ss.str();
    }
    
    virtual bool handle(IRCState &s, IRCMessage &m, std::ostream &o) {
        bool handled = false;
        if(m.command == "NICK") {
            handled = handled || nickChange(s, o, m.trailing);
        }
        else if(m.command == "PRIVMSG") {
            handled = handled
                || doOpAdd(s, m, o)
                || doOpDel(s, m, o);
        }
        return handled;
    }

    virtual std::string get_name() const { return "OPHandler"; }
    
};


extern "C" IRCHandler *create() {
    return new OPHandler();
}

extern "C" void destroy(IRCHandler *h) {
    delete h;
}
