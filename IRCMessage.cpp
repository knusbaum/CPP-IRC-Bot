#include "IRCMessage.hpp"
#include <sstream>

static std::string strip_nick(std::string nick) {
    char y[100] = {0};
    char *yp = y;
    const char *x = nick.c_str();
    
    while(*x != 0 && *x != '!'){
        *yp++ = *x++;
    }

    return std::string(y);
}

// IRC Messages are at most 512 characters.
// Plus 1 char for signal char at front
// Plus 1 char for null terminator.
#define MESSAGE_LEN 514

IRCMessage::IRCMessage(std::istream& in) {
    scan_for_next(in);
    if(! deserialize_field_into(in, 'p', prefix)) { IRCMessage(); return; }
    if(! deserialize_field_into(in, 'c', command)) { IRCMessage(); return; }
    
    char buff[MESSAGE_LEN];
    in.getline(buff, MESSAGE_LEN);
    // using 'a' for 'argument'
    while(buff[0] == 'a') {
        params.push_back(std::string(&buff[1]));
        in.getline(buff, MESSAGE_LEN);
    }

    if(buff[0] != 't') { IRCMessage(); return; }
    trailing = std::string(&buff[1]);
}

void IRCMessage::scan_for_next(std::istream& in) {
    char buff[MESSAGE_LEN];
    do {
        in.getline(buff, MESSAGE_LEN);
    } while(buff[0] != 'b' && in.good());
}

bool IRCMessage::deserialize_field_into(std::istream& in, char indicator, std::string& str) {    
    char buff[MESSAGE_LEN];
    in.getline(buff, MESSAGE_LEN);
    if(buff[0] != indicator) {
        return false;
    }
    str = std::string(&buff[1]);
    return true;
}

std::string IRCMessage::serialize() {
    std::stringstream ss;
    ss << std::string("b") << std::endl;
    ss << std::string("p") << prefix << std::endl;
    ss << std::string("c") << command << std::endl;
    for(auto param : params) {
        ss << std::string("a") << param << std::endl;
    }
    ss << std::string("t") << trailing << std::endl;
    return ss.str();
}

void IRCMessage::print() {
    std::cout << "Message: p[" << prefix << "]";
    std::cout << "c[" << command << "]";
    for(auto par : params) {
        std::cout << "a[" << par << "]";
    }
    std::cout << "t[" << trailing << "]"
              << std::endl;
}

void IRCMessage::reply(std::ostream &o, std::string response) {
    if(params[0][0] != '#') {
        o << "PRIVMSG " << strip_nick(prefix) << " :" << response << "\r\n";
    }
    else {
        o << "PRIVMSG " << params[0] << " :" << response << "\r\n";
    }
}
