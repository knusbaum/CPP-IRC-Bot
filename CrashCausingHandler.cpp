//#ifndef HELLOHANDLER_H
//#define HELLOHANDLER_H

#include "IRCHandler.hpp"

class CrashCausingHandler : public IRCHandler {
public:
    virtual bool handle(IRCState &s, IRCMessage& m, std::ostream& o) {
      if(m.command == "PRIVMSG" && m.trailing == "crash") {
	  int x = 5;
	  int y = x - 5;
	  int z = 5/y;
	  return (z > 0);
        }
        return false;
    }

    virtual std::string get_name() const { return "CrashCausingHandler"; }
};

//#endif

extern "C" IRCHandler *create() {
    return new CrashCausingHandler();
}

extern "C" void destroy(IRCHandler *h) {
    delete h;
}
