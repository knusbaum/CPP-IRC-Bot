#ifndef IRCPARSER_H
#define IRCPARSER_H

#include <iostream>
#include <exception>
#include <vector>
#include "IRCMessage.hpp"

class IRCParser {

public:
    void dump_stream(std::istream& in);
    IRCMessage parse(std::istream& in);

private:
    char Look;
    void get_char(std::istream& in);
    void match(std::istream& in, char c);
    void match_noget(char c);
    void do_prefix(std::istream&, IRCMessage&);
    void do_command(std::istream&, IRCMessage&);
    void do_params(std::istream&, IRCMessage&);
    void do_trailing(std::istream& in, IRCMessage& m);
    void do_middle(std::istream& in, IRCMessage& m);
};

class IRCParseException: public std::exception {
    virtual const char * what() {
        return "Failed to parse IRC Message";
    }
};
    
#endif
