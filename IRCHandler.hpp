#ifndef IRCHANDLER_H
#define IRCHANDLER_H

#include "IRCParser.hpp"
#include "IRCState.hpp"
#include <iostream>

class IRCHandler {
public:
    virtual ~IRCHandler() {}
    virtual bool handle(IRCState &s, IRCMessage& m, std::ostream& o) = 0;    
    virtual std::string get_name() const = 0;
    
    typedef IRCHandler * create_t();
    typedef void destroy_t(IRCHandler *);
};

static std::string strip_nick(std::string nick) {
    char y[100] = {0};
    char *yp = y;
    const char *x = nick.c_str();
    
    while(*x != 0 && *x != '!'){
        *yp++ = *x++;
    }

    return std::string(y);
}

#endif
