#ifndef IRCSTATE_H
#define IRCSTATE_H

#include "IRCMessage.hpp"
#include <iostream>
#include <unordered_map>
#include <set>
#include <string>

class IRCState {
    std::unordered_map<std::string, std::set<std::string>> op_map;
    std::set<std::string> channels;
    std::ostream &o;
    
public:

    IRCState(std::ostream &output);
    
    const std::set<std::string> &get_ops(std::string channel);
    bool is_operator(std::string nick, std::string channel);
    void update_with(IRCMessage &m);
    void request_names();
    void add_channel(std::string channel);
    std::set<std::string> channel_set();
    
private:
    void readusers(IRCMessage& m);
    
};

#endif
