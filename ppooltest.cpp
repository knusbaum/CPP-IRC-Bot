#include <iostream>
#include <memory>
#include <unistd.h>
#include <signal.h>
#include "ProcessPool.hpp"

int main(void) {

    std::cout << "Parent Process Group: " << getpgrp() << std::endl;
    
    ProcessPool *pp = new ProcessPool();
    pp->exec_in_process([](std::shared_ptr<std::istream> in, std::shared_ptr<std::ostream> out) {

            std::cout << "Child Process Group: " << getpgrp() << std::endl;
            while(in->good()) {
                char x[200];
                in->getline(x, 200);
                *out << "GOT:_" << x << std::endl;
            }
        });
    
    std::shared_ptr<Process> child = pp->processes[0];

    for(int i = 0; i < 10 && child->out_stream->good(); i++) {
        (*child->out_stream) << "Hello!" << std::endl;
        std::string s;
        *pp->processes[0]->in_stream >> s;
        std::cout << s << std::endl;
    }

    /**/
    signal(SIGUSR1, [](int x){});
    
    pause();
    std::cout << "Killing children." << std::endl;
    delete pp;
    std::cout << "DONE!" << std::endl;
    pause();
    /**/
}
