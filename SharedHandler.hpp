#ifndef SHAREDHANDLER_H
#define SHAREDHANDLER_H

#include <ostream>
#include "IRCHandler.hpp"
#include "IRCMessage.hpp"
#include "ProcessPool.hpp"

class SharedHandler : public IRCHandler {
    
public:
    SharedHandler(std::string shared_object_file, ProcessPool& p);
    virtual bool handle(IRCState &s, IRCMessage& m, std::ostream& o);
    virtual std::string get_name() const; // { return "PingHandler"; }

private:
    std::shared_ptr<Process> process;
    
};

#endif
