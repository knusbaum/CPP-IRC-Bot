#include "Process.hpp"
#include <signal.h>
#include <sys/wait.h>

bool Process::kill() {
    if(::kill(child_pid, SIGTERM) == 0) {
        return true;
    }
    return false;
}

bool Process::wait() {
    bool wait = true;
    int status = 0;

    while(wait) {
        wait = false;
        
        int res = waitpid(child_pid, &status, 0);

        if(res == -1) {
            if(errno == EINTR) {
                wait = true;
            }
            else {
                return false;
            }
        }
        else if(res == child_pid) {
            return true;
        }
        else {
            return false;
        }
    }

}
