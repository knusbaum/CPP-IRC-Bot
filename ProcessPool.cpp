#include "ProcessPool.hpp"
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include <fstream>
#include <stdio.h>
#include <boost/iostreams/device/file_descriptor.hpp>
#include <boost/iostreams/stream.hpp>


namespace bio = boost::iostreams;

void ProcessPool::exec_in_process(std::function
				  <void(std::shared_ptr<std::istream> in,
					std::shared_ptr<std::ostream> out)> fun,
				  std::shared_ptr<Process> p) {
    if(!group_leader) {
	spawn_group_leader();
    }

    int parent_in[2];
    int parent_out[2];

    pipe(parent_in);
    pipe(parent_out);

    pid_t c = fork();

    if(c == -1) {
	std::cerr << "FAILED TO FORK!" << std::endl;
    }
    else if(c > 0)
    {
	// Parent
	close(parent_in[1]);
	close(parent_out[0]);

	bio::file_descriptor_source in_buff (parent_in[0], bio::close_handle);
	bio::file_descriptor_sink out_buff (parent_out[1], bio::close_handle);

	// FDs will be closed when file_descriptor_sources/sinks are destroyed.
	std::shared_ptr<std::istream> istr =
	  std::make_shared<bio::stream<bio::file_descriptor_source>>
	  (in_buff);

	std::shared_ptr<std::ostream> ostr =
	  std::make_shared<bio::stream<bio::file_descriptor_sink>>
	  (out_buff);

	p->reset(fun, istr, ostr, c);
	//processes.push_back(p);
//        return p;
	//return processes.back();
    }
    else {
	// Child
        int newgid = setpgid(0, gid);
	if(newgid != -1) {
      //	  std::cout << "Asking for gid: " << gid << " got: " << newgid << std::endl;
	}
	else {
	  char * e = strerror(errno);
	  std::cout << "Error: " << e << std::endl;
      return;
	}
	close(parent_in[0]);
	close(parent_out[1]);

	bio::file_descriptor_sink out_buff (parent_in[1], bio::close_handle);
	bio::file_descriptor_source in_buff (parent_out[0], bio::close_handle);

	std::shared_ptr<std::istream> child_istr =
	  std::make_shared<bio::stream<bio::file_descriptor_source>>(in_buff);

	std::shared_ptr<std::ostream> child_ostr =
	  std::make_shared<bio::stream<bio::file_descriptor_sink>>(out_buff);

	fun(child_istr, child_ostr);
	exit(0);
    }
}

std::shared_ptr<Process> ProcessPool::exec_in_process(
    std::function
    <void(std::shared_ptr<std::istream> in,
	  std::shared_ptr<std::ostream> out)> fun) {
    auto p = std::make_shared<Process>(nullptr, nullptr, nullptr, 0);
    processes.push_back(p);
    exec_in_process(fun, processes.back());
    return processes.back();
}

void ProcessPool::spawn_group_leader() {

    int parent_in[2];
    int child_kill[2];

    pipe(parent_in);
    pipe(child_kill);

    pid_t c = fork();
    if(c == -1) {
	std::cerr << "FAILED TO FORK!" << std::endl;
    }
    else if(c > 0)
    {
	// Parent
	close(parent_in[1]);
	close(child_kill[0]);
	FILE *parentin = fdopen(parent_in[0], "r");
	fscanf(parentin, "%ld", &gid);
	fclose(parentin);
	close(parent_in[0]);
	group_leader = new Process(NULL, NULL, NULL, c);
    }
    else {
	// Child
	close(parent_in[0]);
	close(child_kill[1]);
	setpgid(0,0);
	FILE *childout = fdopen(parent_in[1], "w");

	std::cout << "ProcessPool Process Group: " << getpgrp() << std::endl;

	fprintf(childout, "%ld", getpgrp());
	fflush(childout);
	fclose(childout);
	close(parent_in[1]);

	char x[2];
	read(child_kill[0], &x, 1);

	exit(0);
    }

}

void ProcessPool::killall() {
    std::cout << "Killing all processes in group: " << gid << std::endl;
    // Kill all in gid process group.
    ::kill(-gid, SIGTERM);
    int status;
    bool cont = true;
    // While there are still living processes...
    while(group_leader && !processes.empty()) {
        pid_t dead = waitpid(-gid, &status, 0);
        if(group_leader && dead == group_leader->get_pid()) {
            delete group_leader;
            group_leader = NULL;
        }
        else {
            // Find the process and remove it from the vector.
            auto it = processes.begin();
            for(; it != processes.end(); ++it) {
                if( (*it)->get_pid() == dead ) {
                    processes.erase(it);
                    break;
                }
            }
        }
    }
}

ProcessPool::~ProcessPool() {
    // Group leader only exists if we've launched processes.
    // Only clean up if we've launched processes
    if(group_leader) {
      killall();
    }
}

void ProcessPool::respawn() {
    int status, savedgid = gid;
    pid_t flagpole = waitpid(group_leader->get_pid(), &status, WNOHANG);
    if(flagpole != 0) {
        // Uh oh, our group leader has gone down. We need to respawn all the processes in the group.
        std::cout << "ERROR: ProcessPool's group leader died. Restarting pool!" << std::endl;
        ::kill(-gid, SIGKILL);
        spawn_group_leader();
        auto it = processes.begin();
        for(; it != processes.end(); ++it) {
          std::shared_ptr<Process> p = (*it);
          pid_t pid = p->get_pid();
          waitpid(pid, NULL, 0);
          std::cout << "Restarting Process " << p->get_pid() << " ... ";
          exec_in_process(p->get_function(), p);
          std::cout << " new PID: " << p->get_pid() << std::endl;
        }
        return;
    }

    pid_t dead = waitpid(-savedgid, &status, WNOHANG);
    while(dead > 0) {
        std::shared_ptr<Process> p = nullptr;
        auto it = processes.begin();
        for(; it != processes.end(); ++it) {
	    if((*it)->get_pid() == dead) {
		p = (*it);
		break;
	    }
	}

	if(p && (p->get_function() != NULL)) {
        std::cout << "Restarting Process " << p->get_pid() << " ... ";
        exec_in_process(p->get_function(), p);
        std::cout << " new PID: " << p->get_pid() << std::endl;
	}
	dead = waitpid(-gid, &status, WNOHANG);
    }
}
