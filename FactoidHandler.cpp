//#ifndef HELLOHANDLER_H
//#define HELLOHANDLER_H

#include "IRCHandler.hpp"
#include <unordered_map>
#include <string>
#include <fstream>
#include <regex>
#include <set>

class FactoidHandler : public IRCHandler {
    std::unordered_map<std::string, std::string> fact_table;
    std::fstream myfile;
public:

    virtual ~FactoidHandler() {
        myfile.close();
        fact_table.empty();
    }
    
    FactoidHandler() {
        std::cout << get_name() << " Loading ..." << std::endl;
        myfile.open("facts", std::ios::in);
        if(myfile.is_open()) {
            std::string line;
            while(getline(myfile, line)) {
                
                size_t split = line.find(':');
                std::string name = line.substr(0, split);
                std::string fact = line.substr(split + 1);
                std::cout << get_name() << " Loaded Fact [" << name << "] [" << fact << "]" << std::endl;
                fact_table.erase(name);
                fact_table.insert({name, fact});
            }
            std::cout << get_name() << " Done Getting Lines." << std::endl;
        }
        else {
            std::cout << get_name() << " Failed to load facts." << std::endl;
        }

        myfile.close();
        myfile.open("facts", std::ios::out | std::ios::app);
        std::cout << get_name() << " Done." << std::endl;
    }

    std::string get_fact(std::string name) const {
        auto it = fact_table.find(name);
        if(it != fact_table.end()) {
            std::string text = (*it).second;
            return name + ": " + text;
        }
        else {
            return "No such fact '" + name + "'";
        }
    }

    bool dofact(IRCMessage &m, std::ostream& o) const {
        try {
            std::regex re("(fact( +([^ ]*).*)?|,([^ ]*))$");
            std::smatch match;
            if(std::regex_search(m.trailing, match, re)
               && match.size() > 2) {
                std::string factname = match.str(3);
                if(factname.length() == 0) {
                    factname = match.str(4);
                }
                if(factname.length() > 0) {
//                    o << "PRIVMSG " << m.params[0];
                    m.reply(o, get_fact(factname));                   
                }
                else {
                    m.reply(o, "Usage: fact [fact name]");
//                    o << "PRIVMSG " << m.params[0] << " :Usage: fact [fact name]\r\n";
                }
                return true;
            }
        }
        catch (std::regex_error& e) {
            std::cout << get_name() << " Bad Regex." << std::endl;
        }
        return false;
    }

    bool dofactadd(IRCState &s, IRCMessage &m, std::ostream& o) {
        try {
            std::regex re("factadd( +([^ ]*) +(.*))?$");
            std::smatch match;
            if(std::regex_search(m.trailing, match, re)
               && match.size() > 1) {
                std::string factname = match.str(2);
                std::string fact = match.str(3);

                if(factname.size() > 0 && fact.size() > 0) {
                
                    if(s.is_operator(strip_nick(m.prefix), m.params[0])) {
                        
                        std::cout << get_name() << " Adding fact: [" << factname << "] [" << fact << "]" << std::endl;
                        myfile << factname << ":" << fact << std::endl;
                        myfile.flush();
                        fact_table.erase(factname);
                        fact_table.insert({factname, fact});
                        m.reply(o, "Added fact '" + factname + "'");
//                        o << "PRIVMSG " << m.params[0] << " :Added fact '" << factname << "'\r\n";
                    }
                    else {
                        m.reply(o, "Must be an operator to add facts.");
//                        o << "PRIVMSG " << m.params[0] << " :Must be an operator to add facts.\r\n";
                    }
                }
                else {
                    m.reply(o, "Usage: factadd [fact name (no spaces)] [fact]");
//                    o << "PRIVMSG " << m.params[0] << " :Usage: factadd [fact name (no spaces)] [fact]\r\n";
                }
                return true;
            }
        }
        catch (std::regex_error& e) {
            std::cout << get_name() << " Bad Regex." << std::endl;
        }
        return false;
    }
    
    virtual bool handle(IRCState &s, IRCMessage& m, std::ostream& o) {
        bool handled = false;
        if(m.command == "PRIVMSG") {
            handled = handled || dofact(m, o);
            handled = handled || dofactadd(s, m, o);
        }
        return handled;
    }

    virtual std::string get_name() const { return "FactoidHandler"; }
};

//#endif

extern "C" IRCHandler *create() {
    return new FactoidHandler();
}

extern "C" void destroy(IRCHandler *h) {
    delete h;
}
