CXX = clang++
CXXFLAGS = -std=c++11 -gdwarf-2 -I/usr/local/include/
LFLAGS = -lboost_system -lboost_iostreams -lpthread  -Wl,-E -L/usr/local/lib/

OBJECTS = IRCParser.o \
	main.o \
	ProcessPool.o \
	Process.o \
	IRCMessage.o \
	SharedHandler.o \
	IRCState.o

MODULES = HelloHandler.so \
	KickHandler.so \
	CrashCausingHandler.so \
	FactoidHandler.so \
	OPHandler.so

all: irc ppooltest $(MODULES)

irc: $(OBJECTS) 
	$(CXX) $(CXXFLAGS) $(OBJECTS) $(LFLAGS) -o irc

ppooltest: ppooltest.cpp ProcessPool.cpp Process.cpp
	$(CXX) $(CXXFLAGS) $(LFLAGS) ppooltest.cpp ProcessPool.cpp Process.cpp -o ppooltest

clean:
	-@rm *.o *~ *.so irc ppooltest


ProcessPool.o : ProcessPool.hpp Process.hpp ProcessPool.cpp
	$(CXX) $(CXXFLAGS) -c ProcessPool.cpp -o ProcessPool.o

main.o : IRCParser.hpp IRCMessage.hpp IRCState.hpp IRCHandler.hpp PingHandler.hpp NicknameInUseHandler.hpp HelloHandler.cpp KillHandler.hpp main.cpp
	$(CXX) $(CXXFLAGS) -c main.cpp -o main.o

IRCParser.o : IRCParser.hpp IRCParser.cpp IRCMessage.hpp
	$(CXX) $(CXXFLAGS) -c IRCParser.cpp -o IRCParser.o

IRCState.o : IRCState.hpp IRCState.cpp IRCMessage.hpp
	$(CXX) $(CXXFLAGS) -c IRCState.cpp -o IRCState.o

Process.o : Process.hpp Process.cpp
	$(CXX) $(CXXFLAGS) -c Process.cpp -o Process.o

IRCMessage.o : IRCMessage.cpp IRCMessage.hpp
	$(CXX) $(CXXFLAGS) -c IRCMessage.cpp -o IRCMessage.o

dlclass.o : dlclass.cpp dlclass.hpp
	$(CXX) $(CXXFLAGS) -c dlclass.cpp -o dlclass.o

HelloHandler.so : HelloHandler.cpp IRCHandler.hpp
	$(CXX) $(CXXFLAGS) -fPIC -shared HelloHandler.cpp -o HelloHandler.so

KickHandler.so : KickHandler.cpp IRCHandler.hpp
	$(CXX) $(CXXFLAGS) -fPIC -shared KickHandler.cpp -o KickHandler.so

CrashCausingHandler.so : CrashCausingHandler.cpp IRCHandler.hpp
	$(CXX) $(CXXFLAGS) -fPIC -shared CrashCausingHandler.cpp -o CrashCausingHandler.so

FactoidHandler.so : FactoidHandler.cpp IRCHandler.hpp IRCState.hpp IRCState.cpp
	$(CXX) $(CXXFLAGS) -fPIC -shared FactoidHandler.cpp -o FactoidHandler.so

OPHandler.so : OPHandler.cpp IRCHandler.hpp IRCState.hpp IRCState.cpp
	$(CXX) $(CXXFLAGS) -fPIC -shared OPHandler.cpp -o OPHandler.so
