#include "IRCParser.hpp"

void IRCParser::get_char(std::istream& in) {
    in.get(Look);
}

void IRCParser::match(std::istream& in, char c) {
    if(Look == c) {
        get_char(in);
    }
    else {
        throw IRCParseException();
    }
}

void IRCParser::match_noget(char c) {
    if(Look != c) {
        throw IRCParseException();
    }
    Look = 0;
}

void IRCParser::dump_stream(std::istream& in) {
    while(in.good()) {
        get_char(in);
        std::cout << Look;
    }
}

IRCMessage IRCParser::parse(std::istream& in) {
    try {
        IRCMessage m;
        if(Look == 0)
            get_char(in);
        
        if(Look == ':') {
            do_prefix(in, m);
        }

        do_command(in, m);

        do_params(in, m);
        
        while(Look != '\r') {
            get_char(in);
        }

        match(in, '\r');
        match_noget('\n');
        
        return m;
    }
    catch (IRCParseException& e) {
        return IRCMessage();
    }
}

void IRCParser::do_prefix(std::istream& in, IRCMessage& m) {
    match(in, ':');
    while(Look != ' ') {
        m.prefix.push_back(Look);
        get_char(in);
    }
    match(in, ' ');
}

void IRCParser::do_command(std::istream& in, IRCMessage& m) {
    while(Look != ' ') {
        m.command.push_back(Look);
        get_char(in);
    }
}

void IRCParser::do_params(std::istream& in, IRCMessage& m) {
    if(Look == ' ') {
        match(in, ' ');
        if(Look == ':') {
            do_trailing(in, m);
        }
        else {
            do_middle(in, m);
            do_params(in, m);
        }
    }
}

void IRCParser::do_trailing(std::istream& in, IRCMessage& m) {
    match(in, ':');
    while(Look != '\r') {
        m.trailing.push_back(Look);
        get_char(in);
    }
}

void IRCParser::do_middle(std::istream& in, IRCMessage& m) {
    std::string mid;
    while(Look != ' ' && Look != '\r') {
        mid.push_back(Look);
        get_char(in);
    }

    m.params.push_back(mid);
}
