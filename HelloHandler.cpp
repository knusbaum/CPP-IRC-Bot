//#ifndef HELLOHANDLER_H
//#define HELLOHANDLER_H

#include "IRCHandler.hpp"

class HelloHandler : public IRCHandler {
public:
    virtual bool handle(IRCState &s, IRCMessage& m, std::ostream& o) {
        if(m.command == "PRIVMSG" && m.trailing == "hello") {
            std::cout << "Got Message to handle!" << std::endl;
            m.print();
	    if(m.params.size() > 0)
                m.reply(o, "Hello, " + strip_nick(m.prefix));
//	      o << "PRIVMSG " << m.params[0] << " :Hello, " << strip_nick(m.prefix) << "\r\n";
            return true;
        }
        return false;
    }

    virtual std::string get_name() const { return "HelloHandler"; }
};

//#endif

extern "C" IRCHandler *create() {
    return new HelloHandler();
}

extern "C" void destroy(IRCHandler *h) {
    delete h;
}
