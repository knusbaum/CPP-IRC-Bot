#include "IRCState.hpp"
#include <regex>

static std::string strip_nick(std::string nick) {
    char y[100] = {0};
    char *yp = y;
    const char *x = nick.c_str();
    
    while(*x != 0 && *x != '!'){
        *yp++ = *x++;
    }

    return std::string(y);
}

IRCState::IRCState(std::ostream &output)
    : o(output) {}

void IRCState::readusers(IRCMessage& m) {
    try {
        std::set<std::string> &operators = op_map[m.params[2]];
        operators.clear();
        
        std::regex re("(@[^ ]+)");
        std::sregex_iterator next(m.trailing.begin(), m.trailing.end(), re);
        std::sregex_iterator end;
        std::cout << "IRCState Set operators: [" << m.params[2] << "] [";
        for(auto i = next; i != end; ++i) {
            std::smatch match = *i;
            std::string nick = match.str().substr(1);
            std::cout <<  nick << ", ";
            operators.insert(nick);
        }
        std::cout << "]" << std::endl;
    }
    catch (std::regex_error& e) {
        std::cout << "Bad Regex." << std::endl;
    }
}

const std::set<std::string> &IRCState::get_ops(std::string channel) {
    return op_map[channel];
}

bool IRCState::is_operator(std::string nick, std::string channel) {
    std::cout << "Checking operator status of [" << nick << "] in channel [" << channel << "]" << std::endl;

    for(auto it = op_map.begin(); it != op_map.end(); ++it) {
        std::cout << it->first << ": [";
        auto opers = it->second;
        for(auto it2 = opers.begin(); it2 != opers.end(); ++it2) {
            std::cout << *it2 << ", ";
        }
        std::cout << "]" << std::endl;
    }


    std::set<std::string> &operators = op_map[channel];
    
    bool oper = operators.find(nick) != operators.end();
    if(!oper) {
        o << "NAMES " << channel << "\r\n";
    }
    return oper;
}

void IRCState::update_with(IRCMessage &m) {
    if(m.command == "353") {
        readusers(m);
    }
    else if(m.command == "MODE") {
        std::set<std::string> &operators = op_map[m.params[0]];
        if(m.params[1] == "-o") {
            operators.erase(m.params[2]);
            std::cout << "IRCState [" << m.params[0] << "] [" << m.params[2] << "] removed from operator list: [";
            auto it = operators.begin();
            for(; it != operators.end(); ++it) {
                std::cout << *it << ", ";
            }
            std::cout << "]" << std::endl;
        }
        else if(m.params[1] == "+o") {
            operators.insert(m.params[2]);
            std::cout << "IRCState [" << m.params[0] << "] [" << m.params[2] << "] added to operator list: [";
            auto it = operators.begin();
            for(; it != operators.end(); ++it) {
                std::cout << *it << ", ";
            }
            std::cout << "]" << std::endl;
        }
    }
    else if(m.command == "PART") {
        std::set<std::string> &operators = op_map[m.params[0]];
        std::string nick = strip_nick(m.prefix);
        operators.erase(nick);
        std::cout << "IRCState [" << m.params[0] << "] [" << nick << "] removed from operator list: [";
        auto it = operators.begin();
        for(; it != operators.end(); ++it) {
            std::cout << *it << ", ";
        }
        std::cout << "]" << std::endl;
    }
}

void IRCState::add_channel(std::string channel) {
    channels.insert(channel);
}

std::set<std::string> IRCState::channel_set() {
    return channels;
}
